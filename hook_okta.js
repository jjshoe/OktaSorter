function sortApps() {
  var switching = true;

  while (switching) {
    switching = false;

    appInfo = document.getElementsByClassName('app-button-wrapper');

    for (x = 0; x < appInfo.length - 1; x++) {
      var leftAppName = appInfo[x].childNodes[3].innerText;
      var rightAppName = appInfo[x + 1].childNodes[3].innerText;

      if (leftAppName.toLowerCase() > rightAppName.toLowerCase()) {
        switching = true;
        lists = document.getElementsByClassName('app-buttons-list');
        lists[0].insertBefore(appInfo[x + 1], appInfo[x]);
        break;
      }
    }
  }
}

var targetNode = document.body;

// Options for the observer (which mutations to observe)
var config = { attributes: false, childList: true, subtree: true};

// Callback function to execute when mutations are observed
var callback = function(mutationsList, observer) {
    for(var mutation of mutationsList) {
        if (mutation.type == 'childList') {
          sortApps();
        }
    }
};

// Create an observer instance linked to the callback function
var observer = new MutationObserver(callback);

// Start observing the target node for configured mutations
observer.observe(targetNode, config);
